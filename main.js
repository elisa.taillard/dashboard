var express = require('express');
var hostname = 'localhost';
var port = 8081;

var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "dashboard",
    password: "etlf_ng"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

var app = express();

var myRouter = express.Router();

myRouter.route('/user')
    .get(function(req,res){
        con.query('SELECT * FROM dashboard.users', function (err, result, fields) {
            if (err) throw err;
            console.log("Result: " + result[0].user_name);
            console.log(fields);
        });
        res.json({message : "Liste toutes les piscines de Lille Métropole", methode : req.method});
    })
    .post(function(req,res){
        res.json({message : "Ajoute une nouvelle piscine à la liste", methode : req.method});
    })
    .put(function(req,res){
        res.json({message : "Mise à jour des informations d'une piscine dans la liste", methode : req.method});
    })
    .delete(function(req,res){
        res.json({message : "Suppression d'une piscine dans la liste", methode : req.method});
    });

app.use(myRouter);

app.listen(port, hostname, function(){
    console.log("Mon serveur fonctionne sur http://" + hostname + ":" + port);
});