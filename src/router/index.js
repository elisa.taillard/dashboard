import HelloWorld from '../components/HelloWorld.vue'
import NotFound from '../components/NotFoundPage.vue'
import Login from '../components/Login.vue'
import User from '../components/User.vue'
import Overview from '../components/Overview.vue'

const routes = [
  {
    path: '/',
    name: 'HelloWorld',
    component: HelloWorld
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/user',
    name: 'User',
    component: User
  },
  {
    path: '/user/overview',
    name: 'Overview',
    component: Overview
  },
  { path: '*', component: NotFound }
]

export default routes
